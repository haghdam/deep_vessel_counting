__author__ = "Hamed H. Aghdam"
__copyright__ = "2019, The Sonar Project"
__maintainer__ = "Hamed H. Aghdam"
__credits__ = "Hamed H. Aghdam, Reza Habibi"
__email__ = "h.aghdam@uottawa.ca"
__status__ = "Development"

import numpy as np
import random
import os
import threading
import time
from sonardata.sonar_data import SonarData

DS_ROOT_DIR = '/home/{}/Desktop/sonar'.format(os.environ['USER'])
if DS_ROOT_DIR[-1] != '/':
    DS_ROOT_DIR += '/'

PATH_TRAIN = DS_ROOT_DIR + 'dataset/train'
PATH_VAL = DS_ROOT_DIR + 'dataset/val'
PATH_TEST = DS_ROOT_DIR + 'dataset/test'


def dropout(x, p):
    mask = np.random.binomial(1, 1-p, (x.shape[0], 1))
    return x * mask


class ReadBatch:
    def __init__(self, batch_size, data_source):
        names_list = []
        path = os.path.join(data_source)
        for files in os.listdir(path):
            names_list.append(os.path.join(data_source, files))
        self.name_list = names_list
        self.cur_ind = 0
        self.name_list = names_list
        self.batch_size = batch_size
        self.shuffle_flag = False
        self.__epochs = 0
        self.mean = None
        self.scale = None
        self.ignore_ntargs = [0, 4]
        self.ignore_noise_level = None
        self.ntarg_to_label_map = {1: 0,
                                   2: 1,
                                   3: 2,
                                   4: 3}
        self.preprocess_func = None
        self.augment_list = [(dropout, {'p': 0.1})]

        self.augment_probability = 0

    def __len__(self):
        return len(self.name_list)

    def extend(self, data_source):
        path = os.path.join(data_source)
        for files in os.listdir(path):
            self.name_list.append(os.path.join(data_source, files))

    def epoch_count(self):
        return self.__epochs

    def shuffle_data(self):
        random.shuffle(self.name_list)

    def read_batch(self):
        x_batch = np.zeros((self.batch_size, 1024, 500, 1), dtype="float32")
        y_batch = np.zeros((self.batch_size, 1), dtype="int32") + 1e3
        names_list = self.name_list

        i = 0
        while i < self.batch_size:
            if self.cur_ind >= len(names_list):
                self.__epochs += 1
                self.cur_ind = 0
            sonar = SonarData(names_list[self.cur_ind])
            if len(sonar.spec_arr) > 0:
                if sonar.num_targets in self.ignore_ntargs:
                    del self.name_list[self.cur_ind]
                    continue
                if self.ignore_noise_level is not None and sonar.noise_level in self.ignore_noise_level:
                    del self.name_list[self.cur_ind]
                    continue
                x_batch[i, ..., 0] = sonar.spec_arr[0].T
                if self.augment_probability > 0 and random.uniform(0, 1) < self.augment_probability:
                    f, kw = random.sample(self.augment_list, 1)[0]
                    x_batch[i, ..., 0] = f(x_batch[i, ..., 0], **kw)

                y_batch[i, 0] = self.ntarg_to_label_map[int(sonar.num_targets)]

            i += 1
            self.cur_ind += 1

        if self.shuffle_flag and self.cur_ind == 0:
            self.shuffle_data()

        if self.preprocess_func is not None and callable(self.preprocess_func):
            x_batch = self.preprocess_func(x_batch)

        return x_batch, y_batch


class ReadBatchThreaded(threading.Thread):
    def __init__(self, read_batch, max_size=10):
        super(ReadBatchThreaded, self).__init__()
        self.daemon = True

        self.queue = list()
        self.maxSize = max_size
        self.read_batch = read_batch

    def size(self):
        return len(self.queue)

    def enqueue(self, data):
        if self.size() >= self.maxSize:
            return False
        self.queue.append(data)
        return True

    def dequeue(self):
        tries = 0
        while True:
            if len(self.queue) > 0:
                return self.queue.pop()
            time.sleep(0.05)
            tries += 1
            if tries >= 500:
                raise RuntimeError('Could not read data from disk')

    def run(self):
        while True:
            if self.size() < self.maxSize:
                self.enqueue(self.read_batch.read_batch())
            else:
                time.sleep(0.1)
