__author__ = "Hamed H. Aghdam"
__copyright__ = "2019, The Sonar Project"
__maintainer__ = "Hamed H. Aghdam"
__credits__ = "Hamed H. Aghdam, Reza Habibi"
__email__ = "h.aghdam@uottawa.ca"
__status__ = "Development"


import matplotlib.pyplot as plt
import numpy as np
from os import path


class SonarData(object):
    def __init__(self, fname=None, compute_delta=True):
        super(SonarData, self).__init__()
        self.fname = fname
        self.num_targets = None
        self.noise_level = 0
        self.num_resolutions = 0
        self.spec_arr = []
        if fname is not None:
            if path.splitext(fname)[1] == '.npz':
                self.load_npz()
            else:
                raise ValueError('Invalid file')

    def load_npz(self):
        try:
            f_id = np.load(self.fname, encoding='bytes')
            self.num_targets = f_id['num_targets']
            self.noise_level = f_id['noise_level']
            self.num_resolutions = f_id['num_resolutions']
            self.spec_arr = f_id['spec_arr']
        except:
            print(self.fname)
            import os
            os.remove(self.fname)

    def save_npz(self, fname):
        np.savez(fname,
                 num_targets=self.num_targets,
                 noise_level=self.noise_level,
                 num_resolutions=self.num_resolutions,
                 spec_arr=self.spec_arr)

    def visualize(self):
        for i in range(self.num_resolutions):
            mat = self.spec_arr[i]
            plt.figure(i, figsize=(16, 9))

            plt.imshow(10*np.log(mat + 1e-9), interpolation='nearest', cmap='rainbow', vmin=-40, vmax=20)
            plt.xlabel('Freq')
            plt.ylabel('Time')
            plt.title('# of Targets: {}, Spectrum Array Index {}, Noise Level {}'.format(self.num_targets, i, self.noise_level))
            plt.colorbar()
        plt.show()
