# A Deep Neural Network for Counting the Number of Vessels using Sonar Signals
This repository provides codes for reproducing the results in the following paper:
    
    @article{HHA2020,
        title = {A Deep Neural Network for Counting the Number of Vessels using Sonar Signals},
        author = {Hamed H. Aghdam, Martin Bouchard, Robert Laganiere, Emil M. Petriu, Philip Wort},
        booktitle = {Proceedings of  33rd Canadian Conference on Artificial Intelligence},
        year = {2020}
   }

![alt text](resources/network_arch.png "Overview of our method")
Monitoring the oceanographic activity of ships in restricted areas is an important task that can be done using sonar signals. To this end, a human expert may regularly analyze passive sonar signals to count the number of vessels in the region. To automate this process, we propose a deep neural network for counting the number of vessels using sonar signals. Our model is different from common approaches for acoustic signal processing in the sense that it has a rectangular receptive field and utilizes temporal feature integration to perform this task. Moreover, we create a dataset including 117K samples where each sample resembles a scenario with at most 3 vessels. Our results show that the proposed network outperforms traditional methods substantially and classifies 96% of test samples correctly. Also, we extensively analyze the behavior of our network through various experiments.

## Dependencies:
---
* Python 3.6
* Tensorflow > 1.10
* Numpy > 1.13
* Ubuntu > 16 (not tested on other platforms)

## Dataset
---
We have synthesized a dataset and split the it into training (116,032 samples), validation (10,020 samples) and test (27,012 samples) sets. You can download the dataset from this link.

### How to use the datasets
Create a folder on your disk and copy the above dataset into this folder. Then, __change__
the value of "DS_ROOT_DIR" in [read_batch.py](sonardata/read_batch.py#L15) to this folder. Unless you change the codes in this file, it expects the following structure:

    DS_ROOT_DIR >
                |-dataset >                
                |           |- train
                |           |- test
                |           |- val
                
## How to Run
After setting up the dataset, you can simply run `train.py` script to train your network and `evaluate.py` script to evaluate the network. Note that you may need to
change the path to the checkpoint in [evaluate.py](network/evaluate.py#L34).
