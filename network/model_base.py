__author__ = "Hamed H. Aghdam"
__copyright__ = "2019, The Sonar Project"
__maintainer__ = "Hamed H. Aghdam"
__credits__ = "Hamed H. Aghdam"
__email__ = "h.aghdam@uottawa.ca"
__status__ = "Development"

import tensorflow as tf
from network.extended_graphkeys import GraphKeysExtended
from os.path import join, exists
from os import makedirs
from datetime import datetime
import numpy as np
from sonardata.read_batch import ReadBatchThreaded


class NetworkBase(object):
    def __init__(self, batch_size=32, input_shape=(500, 512, 1), n_classes=4):
        self.graph_nodes = {}
        self.batch_size = batch_size
        shp = input_shape
        self.tf_input = tf.placeholder(tf.float32, shape=(batch_size, shp[0], shp[1], shp[2]), name='input')
        self.tf_label = tf.placeholder(tf.int32, shape=(batch_size, 1), name='label')
        self.tf_lr = tf.placeholder(tf.float32, shape=(), name='learning_rate')
        self.tf_is_training = tf.placeholder_with_default(True, (), name='is_training')
        self.tf_keep_prob = tf.placeholder(tf.float32, shape=(), name='keep_prob')
        self.tf_global_iter = tf.placeholder(tf.int32, (), 'current_iter')
        self.num_classes = n_classes

        self.base_lr = 0.01
        self.weight_decay = 1e-5
        self.update_rule = 'momentum'
        self.lr_policy = 'step'
        self.gamma = 0.5
        self.steps = []
        self.max_iter = 1e5
        self.validation_interval = 5000
        self.display_interval = 200
        self.snapshot_interval = 10000
        self.accumulate_gradient_iter = 1

        self.name = 'NetworkBase'

    def __setattr__(self, key, value):
        if key == 'steps' and key in self.__dict__:
            if value[0] != 0:
                value = [0] + value

            if value[-1] != self.max_iter:
                value = value + [self.max_iter]
            self.__dict__['steps'] = value
        else:
            self.__dict__[key] = value

    def __getitem__(self, item):
        return self.get_node(item)

    def __str__(self):
        s = 'Network info:\n'
        for att in sorted(self.__dict__.keys()):
            if not callable(self.__dict__[att]) and not isinstance(self.__dict__[att], dict):
                s += '- {}: {}\n'.format(att, self.__dict__[att])
        return s

    def add_node(self, key, value):
        if key in self.graph_nodes:
            raise ValueError('{} is already in the network')
        self.graph_nodes[key] = value
        return value

    def has_node(self, key):
        return key in self.graph_nodes

    def get_node(self, key):
        if key not in self.graph_nodes:
            raise ValueError('{} does not exist')

        return self.graph_nodes[key]

    def build_network(self):
        raise NotImplementedError()

    def build_loss(self):
        logits = self.get_node(GraphKeysExtended.HA_LOGITS)
        with tf.name_scope('cross_entropy_loss'):
            cross_loss = tf.losses.sparse_softmax_cross_entropy(self.tf_label, logits,
                                                                reduction=tf.losses.Reduction.MEAN)

        with tf.name_scope('regularization_loss'):
            reg_loss = self.weight_decay * tf.add_n(tf.get_collection(tf.GraphKeys.REGULARIZATION_LOSSES))

        with tf.name_scope('loss'):
            loss = cross_loss + reg_loss

        self.add_node(GraphKeysExtended.HA_LOSS_CROSS_ENTROPY, cross_loss)
        self.add_node(GraphKeysExtended.HA_LOSS_REGULARIZATION, reg_loss)
        self.add_node(GraphKeysExtended.HA_LOSS_TOTAL, loss)
        return loss

    def build_summary(self, add_image=True):
        with tf.name_scope('summaries'):
            tf.summary.scalar('loss_total', self[GraphKeysExtended.HA_LOSS_TOTAL])
            tf.summary.scalar('loss_cross', self[GraphKeysExtended.HA_LOSS_CROSS_ENTROPY])
            tf.summary.scalar('loss_reg', self[GraphKeysExtended.HA_LOSS_REGULARIZATION])
            if self.has_node(GraphKeysExtended.HA_CONF_MAT_VARIABLE):
                var = self[GraphKeysExtended.HA_CONF_MAT_VARIABLE]
                with tf.name_scope('precision_recall'):
                    diag = tf.diag_part(var)
                    false_pos_and_true_pos = tf.reduce_sum(var, axis=0)
                    false_neg_and_true_pos = tf.reduce_sum(var, axis=1)
                    precision = tf.divide(diag, false_pos_and_true_pos+1e-9)
                    recall = tf.divide(diag, false_neg_and_true_pos+1e-9)
                for i in range(self.num_classes):
                    tf.summary.scalar('precision_class{}'.format(i), precision[i])
                    tf.summary.scalar('recall_class{}'.format(i), recall[i])
            if add_image:
                tf.summary.image('spectrogram', self.tf_input[..., 0:1], max_outputs=1)

        self.add_node(GraphKeysExtended.HA_SUMMARY_ALL, tf.summary.merge_all())

    def build_metrics(self):
        with tf.variable_scope('confusion_matrix'):
            var = tf.get_variable('conf_mat', (self.num_classes, self.num_classes),
                                  tf.float32,
                                  tf.zeros_initializer,
                                  trainable=False,
                                  collections=[tf.GraphKeys.LOCAL_VARIABLES])

            conf_mat = tf.confusion_matrix(tf.squeeze(self.tf_label, axis=-1),
                                           tf.argmax(self.get_node(GraphKeysExtended.HA_LOGITS), axis=-1),
                                           self.num_classes,
                                           name='conf_mat',
                                           dtype=tf.float32)
            conf_mat_update_op = tf.assign_add(var, conf_mat, name='update_conf_mat')
            conf_mat_init_op = tf.variables_initializer([var])

            self.add_node(GraphKeysExtended.HA_CONF_MAT_VARIABLE, var)
            self.add_node(GraphKeysExtended.HA_CONF_MAT_UPDATE_OP, conf_mat_update_op)
            self.add_node(GraphKeysExtended.HA_CONF_MAT_INIT_OP, conf_mat_init_op)

    def build_train(self):
        if self.update_rule == 'momentum':
            train = tf.train.MomentumOptimizer(self.tf_lr, 0.9)
        elif self.update_rule == 'rms':
            train = tf.train.RMSPropOptimizer(self.tf_lr)
        elif self.update_rule == 'adam':
            train = tf.train.AdamOptimizer(self.tf_lr)
        else:
            raise ValueError('Update rule is not recognized!')

        update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
        if self.accumulate_gradient_iter <= 1:
            train_op = train.minimize(self[GraphKeysExtended.HA_LOSS_TOTAL])
            if update_ops is not None and len(update_ops) > 0:
                train_op = tf.group([train_op, update_ops])
            self.add_node(GraphKeysExtended.HA_TRAIN_OP, train_op)
        else:
            raise NotImplementedError('We removed this part since we did not use gradient accumulation in this project.')

    def build_all(self):
        self.build_network()
        self.build_loss()
        self.build_metrics()
        self.build_summary()
        self.build_train()

    def evaluate(self, sess_or_ckpg, ds_val, verbose=True, is_training=False, return_features=False):
        if not isinstance(sess_or_ckpg, tf.Session):
            config = tf.ConfigProto()
            config.gpu_options.allow_growth = True
            sess = tf.Session(config=config)
            sess.run(tf.global_variables_initializer())
            sess.run(tf.local_variables_initializer())

            restore = tf.train.Saver(tf.global_variables())
            restore.restore(sess, sess_or_ckpg)
            del restore
        else:
            sess = sess_or_ckpg

        sess.run(self[GraphKeysExtended.HA_CONF_MAT_INIT_OP])

        cur_iter = 0
        max_iter = len(ds_val)//self.batch_size
        if verbose:
            print('Started evaluation')

        ds_val.cur_ind = 0
        list_probs = []
        list_logits = []
        list_y_actual = []

        list_features = []
        while cur_iter < max_iter:
            x_batch, y_batch = ds_val.read_batch()

            fetches = [self[GraphKeysExtended.HA_CONF_MAT_UPDATE_OP],
                       self[GraphKeysExtended.HA_CONF_MAT_VARIABLE],
                       self[GraphKeysExtended.HA_LOGITS],
                       self[GraphKeysExtended.HA_PROBABILITY],
                       self['integrate']]
            feed_dict = {self.tf_input: x_batch,
                         self.tf_label: y_batch,
                         self.tf_is_training: is_training,
                         self.tf_keep_prob: 1.0}
            _, conf_mat, logits, probs, feats = sess.run(fetches, feed_dict)
            if verbose and cur_iter % 100 == 0 and cur_iter > 0:
                print('Accuracy until {}: {}'.format(cur_iter,
                                                     np.diag(conf_mat.astype('float32')).sum()/conf_mat.sum()))
            for i in range(probs.shape[0]):
                list_probs.append(probs[i].tolist())
                list_logits.append(logits[i].tolist())
                list_y_actual.append(y_batch[i, 0])
                if return_features:
                    list_features.append(feats[i].squeeze().tolist())
            cur_iter += 1
        conf_mat = sess.run(self[GraphKeysExtended.HA_CONF_MAT_VARIABLE])
        sess.run(self[GraphKeysExtended.HA_CONF_MAT_INIT_OP])
        if not isinstance(sess_or_ckpg, tf.Session):
            sess.close()
        list_probs = np.asarray(list_probs)
        list_logits = np.asarray(list_logits)
        list_y_actual = np.asarray(list_y_actual, dtype='int32')

        if return_features:
            list_features = np.asarray(list_features)
            return conf_mat, list_probs, list_logits, list_y_actual, list_features
        else:
            return conf_mat, list_probs, list_logits, list_y_actual

    def train(self, ds_train, ds_val, log_dir, restore_from=None, restore_kwargs={}):
        if not exists(log_dir):
            makedirs(log_dir)

        with open(join(log_dir, '{}.log'.format(datetime.now())), 'w')as f_id:
            s = str(self)
            f_id.write('{}\n'.format(s))
            print(s)
            del s

        ds_train.cur_ind = 0
        dr_train = ReadBatchThreaded(ds_train, 20)
        dr_train.start()

        cur_iter = 0
        lr = self.base_lr

        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        sess = tf.Session(config=config)
        sess.run(tf.global_variables_initializer())
        sess.run(tf.local_variables_initializer())

        if restore_from is not None:
            var_list = [v for v in tf.global_variables() if v.name.find('Accumulate') < 0]
            restore = tf.train.Saver(var_list)
            restore.restore(sess, restore_from)

            if 'cur_iter' in restore_kwargs:
                cur_iter = restore_kwargs['cur_iter']
            if 'force_test' in restore_kwargs and restore_kwargs['force_test']:
                self.__evaluate_on_train(ds_val, sess)
            del restore

        fw = tf.summary.FileWriter(log_dir, sess.graph, flush_secs=60)
        save = tf.train.Saver(max_to_keep=20)

        while cur_iter < self.max_iter:

            x_batch, y_batch = dr_train.dequeue()
            fetches = [self[GraphKeysExtended.TRAIN_OP],
                       self[GraphKeysExtended.HA_CONF_MAT_UPDATE_OP],
                       self[GraphKeysExtended.HA_LOSS_TOTAL],
                       self[GraphKeysExtended.HA_LOSS_CROSS_ENTROPY],
                       self[GraphKeysExtended.HA_LOSS_REGULARIZATION]
                       ]
            feed_dict = {self.tf_input: x_batch,
                         self.tf_label: y_batch,
                         self.tf_lr: lr,
                         self.tf_is_training: True,
                         self.tf_keep_prob: 0.5}

            if cur_iter % self.display_interval == 0:
                fetches.append(self[GraphKeysExtended.HA_SUMMARY_ALL])
                _, _, loss_tot, loss_cross, loss_reg, summary = sess.run(fetches, feed_dict)
                print('{}: Iter:{}, Total loss: {}, Cross loss:{} Reg. loss: {}, lr: {}'.format(datetime.now(),
                                                                                                cur_iter,
                                                                                                loss_tot,
                                                                                                loss_cross,
                                                                                                loss_reg,
                                                                                                lr))
                fw.add_summary(summary, cur_iter)
                fw.flush()
                sess.run(self[GraphKeysExtended.HA_CONF_MAT_INIT_OP])
            else:
                _, _, loss_tot, loss_cross, loss_reg = sess.run(fetches, feed_dict)

            if cur_iter > 0 and cur_iter % self.snapshot_interval == 0:
                save.save(sess, join(log_dir, '{}-{}.ckpt'.format(self.name, cur_iter)))

            if cur_iter > 0 and cur_iter % self.validation_interval == 0:
                self.__evaluate_on_train(ds_val, sess)

            if self.lr_policy == 'step':
                for pow_i in range(len(self.steps)-1):
                    if self.steps[pow_i] <= cur_iter < self.steps[pow_i + 1]:
                        lr = self.base_lr * pow(self.gamma, pow_i)
                        break
                else:
                    raise ValueError('Could not set the learning rate. Check your steps values')

            cur_iter += 1
        sess.close()

    def __evaluate_on_train(self, ds_val, sess):
        res, _, _, _ = self.evaluate(sess, ds_val, False)
        tp = np.diag(res)
        fp_plus_tp = np.sum(res, axis=0)
        fn_plus_tp = np.sum(res, axis=1)
        pre = tp / fp_plus_tp
        rec = tp / fn_plus_tp
        print('Precision: {}, Recall: {}'.format(pre, rec))
