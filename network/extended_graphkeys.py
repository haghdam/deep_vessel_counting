__author__ = "Hamed H. Aghdam"
__copyright__ = "2019, The Sonar Project"
__maintainer__ = "Hamed H. Aghdam"
__credits__ = "Hamed H. Aghdam"
__email__ = "h.aghdam@uottawa.ca"
__status__ = "Development"

import tensorflow as tf


class GraphKeysExtended(tf.GraphKeys):
    HA_LOGITS = 'logits'
    HA_PROBABILITY = 'probs'
    HA_LOSS_TOTAL = 'loss_total'
    HA_LOSS_CROSS_ENTROPY = 'loss_cross'
    HA_LOSS_REGULARIZATION = 'loss_regularization'
    HA_TRAIN_OP = 'train_op'
    HA_CONF_MAT_VARIABLE = 'conf_mat_variable'
    HA_CONF_MAT_UPDATE_OP = 'conf_mat_update_op'
    HA_CONF_MAT_INIT_OP = 'conf_mat_init_op'
    HA_SUMMARY_ALL = 'summary_all'
    HA_UPDATE_OPS = 'update_ops'
    HA_ACCUMULATE_GRADS_UPDATE = 'accumulate_grads_update'
    HA_ACCUMULATE_GRADS_INIT = 'accumulate_grads_init'
    HA_ACCUMULATE_GRADS_VARS = 'accumulate_grads_vars'