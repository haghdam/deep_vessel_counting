__author__ = "Hamed H. Aghdam"
__copyright__ = "2019, The Sonar Project"
__maintainer__ = "Hamed H. Aghdam"
__credits__ = "Hamed H. Aghdam"
__email__ = "h.aghdam@uottawa.ca"
__status__ = "Development"

from sonardata.read_batch import ReadBatch, PATH_TRAIN, PATH_VAL, DS_ROOT_DIR
from network.wrn import VesselCount
import os


os.environ['CUDA_VISIBLE_DEVICES'] = '1'

batch_size = 32
ds_train = ReadBatch(batch_size, PATH_TRAIN)
ds_train.shuffle_flag = True
ds_train.shuffle_data()
ds_train.augment_probability = 0.0

ds_val = ReadBatch(batch_size, PATH_VAL)
ds_val.shuffle_flag = False

ds_train.preprocess_func = ds_val.preprocess_func = None

net = VesselCount(batch_size, (1024, 500, 1), n_classes=3)
iter_per_epoch = 80000//batch_size

net.base_lr = 0.001
net.weight_decay = 1e-4
net.update_rule = 'momentum'
net.lr_policy = 'step'
net.gamma = 0.1
net.max_iter = iter_per_epoch * 75
net.steps = [iter_per_epoch * 40, iter_per_epoch * 60]
net.validation_interval = 5000
net.display_interval = 200
net.snapshot_interval = 5000

net.build_all()
net.train(ds_train, ds_val, '{}{}'.format(DS_ROOT_DIR, net.name)
          # , restore_from='PATH TO YOU CKPT FILE'
          # , restore_kwargs={'cur_iter': 0, 'force_test': False}
          )
