__author__ = "Hamed H. Aghdam"
__copyright__ = "2019, The Sonar Project"
__maintainer__ = "Hamed H. Aghdam"
__credits__ = "Hamed H. Aghdam"
__email__ = "h.aghdam@uottawa.ca"
__status__ = "Development"

from sonardata.read_batch import ReadBatch, DS_ROOT_DIR, PATH_TRAIN, PATH_TEST
from network.wrn import VesselCount
import os
import numpy as np
from sklearn.metrics import precision_recall_curve, confusion_matrix
import matplotlib.pyplot as plt

os.environ['CUDA_VISIBLE_DEVICES'] = '1'

batch_size = 37

ds_test = ReadBatch(batch_size, PATH_TEST)
ds_test.shuffle_flag = False

ds_test.preprocess_func = None

fname = 'prob_logits_yact_confmat_'
root = DS_ROOT_DIR

force_evaluate = False
if force_evaluate:
    net = VesselCount(batch_size, (1024, 500, 1), n_classes=3)
    net.build_network()
    net.build_metrics()

    # ds_val.ignore_noise_level = [0.2, 0.4, 0.6, 0.8]
    conf_mat, probs, logits, y_act = net.evaluate(root + 'VesselCount/VesselCount-187000.ckpt', ds_test)
    tp = np.diag(conf_mat)
    fp_plus_tp = np.sum(conf_mat, axis=0)
    fn_plus_tp = np.sum(conf_mat, axis=1)
    pre = tp / fp_plus_tp
    rec = tp / fn_plus_tp
    print('Precision: {}, Recall: {}'.format(pre, rec))
    np.set_printoptions(2)
    print(conf_mat)
    print(conf_mat.sum())
    np.savez(root + fname, probs=probs, logits=logits, y_act=y_act, conf_mat=conf_mat)
else:
    f_id = np.load(root + fname + '.npz')
    probs = f_id['probs']
    y_act = f_id['y_act']
    conf_mat = confusion_matrix(y_act, np.argmax(probs, 1))

print(conf_mat)
tp = np.diag(conf_mat)
fp_plus_tp = np.sum(conf_mat, axis=0)
fn_plus_tp = np.sum(conf_mat, axis=1)
pre = tp / fp_plus_tp
rec = tp / fn_plus_tp
print('\u2014'*28)
print('Precision\tRecall\tSupport\t\t')
print('\u2014'*43)
for i in range(len(pre)):
    print('{0:.3f}\t\t{1:.3f}\t{2}\t\t{3} Target(s)'.format(pre[i], rec[i], conf_mat[i].sum(), i+1))
print('\u2014'*43)
print('Accuracy: {0:.3f}'.format(tp.sum()/conf_mat.sum()))
print('\u2014'*43)

plt.figure(1, figsize=(16, 9), facecolor='w')
colors = ['darkviolet', 'orange', 'teal']
for i in range(probs.shape[1]):
    y_act_bin = np.where(y_act == i, 1, 0)
    pre, rec, thre = precision_recall_curve(y_act_bin, probs[:, i], 1)
    plt.plot(rec, pre, c=colors[i], lw=4, alpha=0.6, label='{} target(s)'.format(i+1))
plt.legend(prop={'family': 'FreeSerif', 'size': 18})
plt.xlabel('Recall', family='FreeSerif', fontsize=18)
plt.ylabel('Precision', family='FreeSerif', fontsize=18)
plt.xticks(family='FreeSerif', size=17)
plt.yticks(family='FreeSerif', size=17)
plt.subplots_adjust(left=0.11, right=0.97, top=0.94, bottom=0.14)
plt.grid(True)

conf_mat_normal = conf_mat / np.sum(conf_mat, axis=1, keepdims=True)
plt.figure(2)
plt.imshow(conf_mat_normal, interpolation='nearest', cmap='GnBu')
plt.colorbar()
plt.xticks([])
plt.yticks([])
plt.xlabel('Predicted', family='FreeSerif', fontsize=18)
plt.ylabel('Actual', family='FreeSerif', fontsize=18)
plt.show()
