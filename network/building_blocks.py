__author__ = "Hamed H. Aghdam"
__copyright__ = "2019, The Sonar Project"
__maintainer__ = "Hamed H. Aghdam"
__credits__ = "Hamed H. Aghdam"
__email__ = "h.aghdam@uottawa.ca"
__status__ = "Development"

import tensorflow as tf


class BuildingBlocks(object):
    @staticmethod
    def wrn_residual_block(incoming, num_filters, kernel_size, stride, name_prefix, keep_prob=1, is_training=True, use_vertical_rf=False):
        with tf.variable_scope(name_prefix):
            bn1 = tf.layers.batch_normalization(incoming, training=is_training, name="bn1")
            relu1 = tf.nn.relu(bn1)
            c1 = tf.layers.conv2d(relu1, num_filters, kernel_size, stride, padding='same',
                                  activation=None, use_bias=False, kernel_initializer=tf.glorot_normal_initializer,
                                  kernel_regularizer=tf.nn.l2_loss, name="conv1")

            bn2 = tf.layers.batch_normalization(c1, training=is_training, name="bn2")
            relu2 = tf.nn.relu(bn2)
            if keep_prob != 1:
                shp = relu2.shape.as_list()
                drop = tf.layers.dropout(relu2, 1 - keep_prob, noise_shape=(shp[0], 1, 1, shp[-1]), training=is_training, name='dropout')
            else:
                drop = relu2

            c2 = tf.layers.conv2d(drop,
                                  num_filters,
                                  (3, 3) if not use_vertical_rf else (3, 1),
                                  strides=1,
                                  padding='same',
                                  activation=None, use_bias=False, kernel_initializer=tf.glorot_normal_initializer,
                                  kernel_regularizer=tf.nn.l2_loss, name="conv2")

            if incoming.shape.as_list() != c2.shape.as_list():
                c_1x1 = tf.layers.conv2d(incoming, num_filters, 1, stride, padding='same',
                                         activation=None, use_bias=False,
                                         kernel_initializer=tf.glorot_normal_initializer,
                                         kernel_regularizer=tf.nn.l2_loss, name="conv_fuse")
                c2 += c_1x1
            else:
                c2 += incoming
        return c2

    @staticmethod
    def wrn_macro_block(nb_layers, incoming, num_filters, kernel_size, stride, name_suffix, keep_prob=1, is_training=True, use_vertical_rf=False):
        node = None
        list_node = []
        with tf.variable_scope("Macro_block" + name_suffix):
            for i in range(nb_layers):
                node = BuildingBlocks.wrn_residual_block(incoming if i == 0 else node,
                                                         num_filters,
                                                         kernel_size if i == 0 else (3, 3),
                                                         stride if i == 0 else 1,
                                                         "WRN{}".format(i + 1),
                                                         keep_prob,
                                                         is_training,
                                                         use_vertical_rf)
                list_node.append(node)
        return list_node