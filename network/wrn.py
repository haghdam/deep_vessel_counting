__author__ = "Hamed H. Aghdam"
__copyright__ = "2019, The Sonar Project"
__maintainer__ = "Hamed H. Aghdam"
__credits__ = "Hamed H. Aghdam"
__email__ = "h.aghdam@uottawa.ca"
__status__ = "Development"

from network.building_blocks import BuildingBlocks
from network.model_base import NetworkBase
import tensorflow as tf
from network.extended_graphkeys import GraphKeysExtended
import sys

if sys.version_info[0] < 3:
    raise ValueError('You must use python > 3.5')


class VesselCount(NetworkBase):
    def __init__(self, batch_size=32, input_shape=(500, 512, 1), n_classes=4):
        super(VesselCount, self).__init__(batch_size, input_shape, n_classes)
        self.steps = [0, 100000, 300000, self.max_iter]
        self.name = 'VesselCount'

    def build_network(self):
        keep_prob = self.tf_keep_prob
        wrn = BuildingBlocks.wrn_macro_block
        n = 3
        k = 2
        bn_init = tf.layers.batch_normalization(self.tf_input, training=self.tf_is_training, name="bn_init",
                                                scale=False, center=False)
        node = tf.layers.conv2d(bn_init, 16, 3, 2, padding='same',
                                activation=None, use_bias=False, kernel_initializer=tf.initializers.glorot_normal,
                                kernel_regularizer=tf.nn.l2_loss, name="conv_init")

        kwargs = {'keep_prob': keep_prob + 0.45, 'is_training': self.tf_is_training}
        node = self.add_node('wrn1', wrn(n, node, 16 * k, kernel_size=(7, 3), stride=(5, 2), name_suffix='_1', **kwargs))
        kwargs = {'keep_prob': keep_prob + 0.4, 'is_training': self.tf_is_training}
        node = self.add_node('wrn2', wrn(n, node[-1], 32 * k, kernel_size=(7, 1), stride=(5, 1), name_suffix='_2', **kwargs))
        kwargs = {'keep_prob': keep_prob + 0.3, 'is_training': self.tf_is_training}
        node = self.add_node('wrn3', wrn(n, node[-1], 64 * k, kernel_size=(7, 1), stride=(5, 1), name_suffix='_3', **kwargs))
        kwargs = {'keep_prob': keep_prob + 0.2, 'is_training': self.tf_is_training}
        node = self.add_node('wrn4', wrn(n, node[-1], 128 * k, kernel_size=(7, 1), stride=(5, 1), name_suffix='_4', **kwargs))
        kwargs['keep_prob'] = keep_prob
        node = self.add_node('wrn5', wrn(n, node[-1], 128 * k, kernel_size=(3, 1), stride=(2, 1), name_suffix='_5', **kwargs))

        with tf.name_scope('Integrate'):
            node = node[-1]
            node = tf.nn.relu(node)
            node = tf.reduce_max(node, axis=2, keepdims=True)
            self.add_node('integrate', node)

        with tf.variable_scope('Logits'):
            node = tf.layers.conv2d(node, self.num_classes, 1, 1, padding='same',
                                    activation=None, use_bias=True,
                                    kernel_initializer=tf.initializers.glorot_normal,
                                    bias_initializer=tf.initializers.zeros,
                                    kernel_regularizer=tf.nn.l2_loss, name="logits")
            node = tf.squeeze(node)
        self.add_node(GraphKeysExtended.HA_LOGITS, node)
        self.add_node(GraphKeysExtended.HA_PROBABILITY, tf.nn.softmax(node))
